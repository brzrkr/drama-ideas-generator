import random


SITUATIONS = (
    "1. Supplica",
    "2. Liberazione/salvataggio",
    "3. Vendetta",
    "4. Vendetta all'interno di una famiglia",
    "5. Il perseguitato, il fuggitivo, l'outsider",
    "6. Disastro",
    "7. Crudeltà e sventura",
    "8. Rivolta",
    "9. Impresa audace/avventura coraggiosa",
    "10. Rapimento",
    "11. Enigma",
    "12. Ottenere",
    "13. Ostilità tra parenti/guerra familiare",
    "14. Rivalità tra parenti",
    "15. Crimini passionali",
    "16. Follia",
    "17. Imprudenza fatale",
    "18. Coincidenza",
    "19. Stato sognante",
    "20. Sacrificarsi per un ideale",
    "21. Sacrificarsi per la famiglia",
    "22. Sacrificare tutto per la passione",
    "23. Sacrificare la persona amata",
    "24. Rivalità tra superiore e inferiore",
    "25. Adulterio",
    "26. Incesto",
    "27. Scoprire il disonore di un amato",
    "28. L'amore ostacolato",
    "29. Amare il proprio nemico",
    "30. Ambizione",
    "31. Conflitti con il potere",
    "32. Gelosia ingiustificata",
    "33. Errore di valutazione",
    "34. Rimorso",
    "35. Ritrovare una persona perduta",
    "36. Perdere una persona amata",
)
"""Italian translation by Giuseppe Contarino."""


def generate_situation() -> str:
    """Pick one of the 36 dramatic situations according to Mike Figgis."""
    return random.choice(SITUATIONS)


if __name__ == '__main__':
    while not input("ENTER per generare..."):
        print(generate_situation())

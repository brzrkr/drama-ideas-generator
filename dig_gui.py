from dig import generate_situation, SITUATIONS
from PyQt5.QtWidgets import (
    QApplication,
    QWidget,
    QHBoxLayout,
    QPushButton,
    QLineEdit,
)
from functools import reduce


def generate_new():
    output.setText(generate_situation())


app = QApplication([])
window = QWidget()
window.setWindowTitle("Generatore di situazioni drammatiche")
layout = QHBoxLayout()

output = QLineEdit(generate_situation())
output.setReadOnly(True)
# Set size according to longest possible output
font_size = output.font().pointSize()
output_size = reduce(max, map(len, SITUATIONS)) * font_size
output.setFixedWidth(output_size)

generate_button = QPushButton("Un'altra")
generate_button.clicked.connect(generate_new)

layout.addWidget(output)
layout.addWidget(generate_button)
window.setLayout(layout)


if __name__ == '__main__':
    window.show()
    app.exec()
